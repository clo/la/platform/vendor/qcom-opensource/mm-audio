/***
Module_codec_control.h -- Pulseaudio module codec control headerfiles.

Copyright (c) 2016-2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/
#ifndef MODULE_CODEC_CONTROL_H
#define MODULE_CODEC_CONTROL_H

#include <pulsecore/modargs.h>
#include <pulsecore/queue.h>
#include <pulsecore/core.h>
#include <pulsecore/idxset.h>
#include <pulse/xmalloc.h>
#include <dlfcn.h>
#include <pulsecore/protocol-native.h>
#include <pulsecore/socket-client.h>
#include <pulsecore/core-util.h>
#include <pulsecore/tagstruct.h>
#include <pulsecore/pstream.h>
#include <pulsecore/pstream-util.h>

#include "audio_hal_plugin.h"

typedef int32_t (*audio_hal_plugin_init_t)(void);
typedef int32_t (*audio_hal_plugin_deinit_t)(void);
typedef int32_t (*audio_hal_plugin_send_msg_t)(audio_hal_plugin_msg_type_t, void *, uint32_t);

struct cc_userdata {
    pa_core *core;
    pa_module *module;
    pa_hook_slot
        *sink_input_put_hook_slot,
        *source_output_put_hook_slot,
        *sink_input_unlink_hook_slot,
        *source_output_unlink_hook_slot,
        *sink_input_move_finish_hook_slot,
        *sink_state_changed_hook_slot,
        *source_output_move_finish_hook_slot,
        *source_state_changed_hook_slot;
    pa_time_event *save_time_event;
    /*role of media*/
    char *role;
    pa_native_protocol *protocol;

    /* a pointer to audio_hal_plugin*/
    void                          *plugin_handle;
    audio_hal_plugin_init_t        audio_hal_plugin_init;
    audio_hal_plugin_deinit_t      audio_hal_plugin_deinit;
    audio_hal_plugin_send_msg_t    audio_hal_plugin_send_msg;

    /* a list of codecs */
    struct pa_idxset *codec_list;
};

typedef enum {
    AUDIO_PLAYBACK = 0,
    AUDIO_CAPTURE
}cc_stream_direction_t;

typedef enum {
    DEFAULT_PLAYBACK_PATH = 0,
    DEFAULT_RECORD_PATH,
    NAVIGATION_PATH,
    HFP_DOWNLINK_PATH,
    HFP_UPLINK_PATH,
    A2DP_PATH,
    SDARS_PATH,
    ICC_CALL_PATH,
    VOICE_ECNS_PATH,
    VOICE_ECNS_REF_PATH,
    MAX_PATH
}cc_path_type_t;


typedef struct {
    cc_stream_direction_t dir;
    cc_path_type_t path;
    const pa_object *object;
}cc_path_desc_t;

#endif
