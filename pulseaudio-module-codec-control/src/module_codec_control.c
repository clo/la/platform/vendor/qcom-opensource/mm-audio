/***
Module_codec_control.c -- Pulseaudio module codec control is a module
belongs to pulseaudio, which could be loaded by pulseaudio.It used to
enable, disable, postprocess configure for codecs.

Copyright (c) 2016-2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#include "module_codec_control.h"
#define LIB_PLUGIN_DRIVER "libaudiohalplugin.so"

PA_MODULE_AUTHOR("AGL Audio");
PA_MODULE_DESCRIPTION("Codec Control Layer");
PA_MODULE_VERSION(PACKAGE_VERSION);
PA_MODULE_LOAD_ONCE(false);

/* Protocol extension commands */
enum {
    SUBCOMMAND_TEST,
    SUBCOMMAND_SET_PP_VOLUME,
    SUBCOMMAND_SET_PP_MUTE,
    SUBCOMMAND_SET_PP_FADE,
    SUBCOMMAND_SET_PP_BALANCE,
    SUBCOMMAND_SET_PP_BMT,
    SUBCOMMAND_SET_PP_EQ,
    SUBCOMMAND_GET_PP_VOLUME,
    SUBCOMMAND_GET_PP_FADE,
    SUBCOMMAND_GET_PP_BALANCE,
    SUBCOMMAND_GET_PP_BMT,
    SUBCOMMAND_GET_PP_EQ,
    SUBCOMMAND_GET_PP_EQ_SUBBANDS,
    SUBCOMMAND_TUNNEL_CMD,
};

#define EXT_VERSION 1

static unsigned int codec_control_hash_func(cc_path_desc_t *p) {
    return ((p->dir & 0x3) << 5) | (p->path & 0x1F);
}

static int codec_control_compare_func(cc_path_desc_t *a, cc_path_desc_t *b) {
    return ((a->dir == b->dir) && (a->path == b->path)) ? 0 : -1;
}

static cc_path_type_t audio_codec_get_path(cc_stream_direction_t dir,
                                                             const char *media_role) {
    cc_path_type_t path;

    if (dir == AUDIO_PLAYBACK){
        if (media_role == NULL) {
            path = DEFAULT_PLAYBACK_PATH;
        } else if (pa_streq(media_role, "phone")) {
            path = DEFAULT_PLAYBACK_PATH;
        } else if (pa_streq(media_role, "video")) {
            path = DEFAULT_PLAYBACK_PATH;
        } else if (pa_streq(media_role, "music")) {
            path = DEFAULT_PLAYBACK_PATH;
        } else if (pa_streq(media_role, "event")) {
            path = DEFAULT_PLAYBACK_PATH;
        } else if (pa_streq(media_role, "navi")) {
            path = NAVIGATION_PATH;
        } else if ((pa_streq(media_role, "hfp_downlink")) || (pa_streq(media_role, "hfp_uplink"))
                  || (pa_streq(media_role, "hfp_wb_downlink")) || (pa_streq(media_role, "hfp_wb_uplink"))) {
            path = HFP_DOWNLINK_PATH;
        } else if (pa_streq(media_role, "sdars_tuner_local_amp")) {
            path = SDARS_PATH;
        } else if (pa_streq(media_role, "icc_call")) {
            path = ICC_CALL_PATH;
        } else {
            path = DEFAULT_PLAYBACK_PATH;
        }
   } else {
        if (media_role == NULL) {
            path = DEFAULT_RECORD_PATH;
        } else if (pa_streq(media_role, "phone")) {
            path = DEFAULT_RECORD_PATH;
        } else if (pa_streq(media_role, "video")) {
            path = DEFAULT_RECORD_PATH;
        } else if (pa_streq(media_role, "music")) {
            path = DEFAULT_RECORD_PATH;
        } else if (pa_streq(media_role, "event")) {
            path = DEFAULT_RECORD_PATH;
        } else if ((pa_streq(media_role, "hfp_downlink")) || (pa_streq(media_role, "hfp_uplink"))
                  || (pa_streq(media_role, "hfp_wb_downlink")) || (pa_streq(media_role, "hfp_wb_uplink"))) {
            path = HFP_UPLINK_PATH;
        } else if (pa_streq(media_role, "sdars_tuner_local_amp")) {
            path = SDARS_PATH;
        } else if (pa_streq(media_role, "icc_call")) {
            path = ICC_CALL_PATH;
        } else if (pa_streq(media_role, "voice_ecns")){
            path = VOICE_ECNS_PATH;
		} else if (pa_streq(media_role, "voice_ecns_ref")){
            path = VOICE_ECNS_REF_PATH;
        } else {
            path = DEFAULT_RECORD_PATH;
        }
    }
    return path;
}

static audio_hal_plugin_direction_type_t audio_codec_get_ext_direction (cc_stream_direction_t dir) {
    audio_hal_plugin_direction_type_t direction = AUDIO_HAL_PLUGIN_DIRECTION_INVALID;
    switch (dir) {
        case AUDIO_PLAYBACK:
            return AUDIO_HAL_PLUGIN_DIRECTION_PLAYBACK;
        case AUDIO_CAPTURE:
            return AUDIO_HAL_PLUGIN_DIRECTION_CAPTURE;
        default:
            return AUDIO_HAL_PLUGIN_DIRECTION_INVALID;
    }
}

static audio_hal_plugin_usecase_type_t audio_codec_get_ext_usecase(cc_path_type_t path) {
    audio_hal_plugin_usecase_type_t usecase = AUDIO_HAL_PLUGIN_USECASE_INVALID;

    switch (path) {
            case DEFAULT_PLAYBACK_PATH:
                usecase =  AUDIO_HAL_PLUGIN_USECASE_DEFAULT_PLAYBACK;
                break;
            case DEFAULT_RECORD_PATH:
                usecase =  AUDIO_HAL_PLUGIN_USECASE_DEFAULT_CAPTURE;
                break;
            case NAVIGATION_PATH:
                usecase =  AUDIO_HAL_PLUGIN_USECASE_DRIVER_SIDE_PLAYBACK;
                break;
            case HFP_DOWNLINK_PATH:
            case HFP_UPLINK_PATH:
                usecase =  AUDIO_HAL_PLUGIN_USECASE_HFP_VOICE_CALL;
                break;
            case SDARS_PATH:
                usecase =  AUDIO_HAL_PLUGIN_USECASE_SDARS_PLAYBACK;
                break;
            case ICC_CALL_PATH:
                usecase = AUDIO_HAL_PLUGIN_USECASE_ICC;
                break;
            case VOICE_ECNS_PATH:
                usecase = AUDIO_HAL_PLUGIN_USECASE_EC_CAPTURE;
                break;
            case VOICE_ECNS_REF_PATH:
                usecase = AUDIO_HAL_PLUGIN_USECASE_EC_REF_CAPTURE;
                break;
            default:
                pa_log_error("%s no supported path", __FUNCTION__);
                break;
        }
    return usecase;
}

static int audio_codec_set_enable(pa_core *c, void *pad, struct cc_userdata *u,
                                                     audio_hal_plugin_direction_type_t dir,
                                                        audio_hal_plugin_usecase_type_t usecase) {
    audio_hal_plugin_codec_enable_t codec_enable;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_ENABLE;
    int ret = 0;

    codec_enable.usecase = usecase;
    codec_enable.direction = dir;
    codec_enable.sample_rate = 48000;
    codec_enable.bit_width = 16;
    codec_enable.num_chs = 2;
    ret = u->audio_hal_plugin_send_msg(msg, (void*)&codec_enable, sizeof(codec_enable));
    if (ret) {
        pa_log_error("%s failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    return ret;
}

static int audio_codec_set_disable(pa_core *c, void *pad, struct cc_userdata *u,
         audio_hal_plugin_direction_type_t dir,
        audio_hal_plugin_usecase_type_t usecase) {
    audio_hal_plugin_codec_disable_t codec_disable;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_DISABLE;
    int ret = 0;

    codec_disable.usecase = usecase;
    codec_disable.direction = dir;
    ret = u->audio_hal_plugin_send_msg(msg, (void*)&codec_disable, sizeof(codec_disable));
    if (ret) {
        pa_log_error("%s failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    return ret;
}

static int audio_codec_set_pp_mute(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                bool mute,
                                                audio_channel_mask_t ch_mask) {
    audio_hal_plugin_codec_set_pp_mute_t pp_mute;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_SET_PP_MUTE;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_mute.direction = dir;
    pp_mute.usecase = usecase;
    pp_mute.flag  = mute;
    pp_mute.ch_mask = ch_mask;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_mute, sizeof(pp_mute));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_set_pp_volume(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                uint32_t gain,
                                                audio_channel_mask_t ch_mask) {
    audio_hal_plugin_codec_set_pp_vol_t pp_vol;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_SET_PP_VOLUME;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_vol.direction = dir;
    pp_vol.usecase = usecase;
    pp_vol.gain  = gain;
    pp_vol.ch_mask = ch_mask;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_vol, sizeof(pp_vol));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_get_pp_volume(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                audio_channel_mask_t ch_mask,
                                                audio_hal_plugin_buint32_t *ret_gain) {
    audio_hal_plugin_codec_get_pp_vol_t pp_vol;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_GET_PP_VOLUME;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_vol.direction = dir;
    pp_vol.usecase = usecase;
    pp_vol.ch_mask = ch_mask;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_vol, sizeof(pp_vol));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    memcpy(ret_gain, &pp_vol.ret_gain, sizeof(audio_hal_plugin_buint32_t));
    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_set_pp_fade(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                int32_t fade) {
    audio_hal_plugin_codec_set_pp_fade_t pp_fade;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_SET_PP_FADE;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_fade.direction = dir;
    pp_fade.usecase = usecase;
    pp_fade.fade  = fade;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_fade, sizeof(pp_fade));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_get_pp_fade(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                audio_channel_mask_t ch_mask,
                                                audio_hal_plugin_bint32_t *ret_fade) {
    audio_hal_plugin_codec_get_pp_fade_t pp_fade;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_GET_PP_FADE;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_fade.direction = dir;
    pp_fade.usecase = usecase;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_fade, sizeof(pp_fade));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    memcpy(ret_fade, &pp_fade.ret_fade, sizeof(audio_hal_plugin_bint32_t));
    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_set_pp_balance(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                int32_t balance) {
    audio_hal_plugin_codec_set_pp_balance_t pp_balance;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_SET_PP_BALANCE;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_balance.direction = dir;
    pp_balance.usecase = usecase;
    pp_balance.balance= balance;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_balance, sizeof(pp_balance));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_get_pp_balance(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                audio_channel_mask_t ch_mask,
                                                audio_hal_plugin_bint32_t *ret_balance) {
    audio_hal_plugin_codec_get_pp_balance_t pp_balance;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_GET_PP_BALANCE;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_balance.direction = dir;
    pp_balance.usecase = usecase;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_balance, sizeof(pp_balance));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    memcpy(ret_balance, &pp_balance.ret_balance, sizeof(audio_hal_plugin_bint32_t));
    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}


static int audio_codec_set_pp_bmt(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                audio_hal_plugin_codec_pp_filter_type_t filter_type,
                                                bool enable,
                                                int32_t value) {
    audio_hal_plugin_codec_set_pp_bmt_t pp_bmt;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_SET_PP_BMT;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_bmt.direction = dir;
    pp_bmt.usecase = usecase;
    pp_bmt.enable_flag = enable;
    pp_bmt.filter_type = filter_type;
    pp_bmt.value = value;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_bmt, sizeof(pp_bmt));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}


static int audio_codec_get_pp_bmt(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                audio_hal_plugin_codec_pp_filter_type_t filter_type,
                                                audio_hal_plugin_bint32_t *ret_value) {
    audio_hal_plugin_codec_get_pp_bmt_t pp_bmt;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_GET_PP_BMT;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_bmt.direction = dir;
    pp_bmt.usecase = usecase;
    pp_bmt.filter_type = filter_type;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_bmt, sizeof(pp_bmt));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    memcpy(ret_value, &pp_bmt.ret_value, sizeof(audio_hal_plugin_bint32_t));
    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_set_pp_eq(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                bool enable,
                                                int32_t preset_id,
                                                uint32_t num_bands,
                                                audio_hal_plugin_codec_pp_eq_subband_t *subband) {
    audio_hal_plugin_codec_set_pp_eq_t pp_eq;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_SET_PP_EQ;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_eq.direction = dir;
    pp_eq.usecase = usecase;
    pp_eq.enable_flag = enable;
    pp_eq.num_bands = num_bands;
    pp_eq.preset_id = preset_id;
    pp_eq.bands = subband;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_eq, sizeof(pp_eq));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_get_pp_eq(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                audio_hal_plugin_bint32_t *ret_value,
                                                uint32_t *ret_num_bands) {
    audio_hal_plugin_codec_get_pp_eq_t pp_eq;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_GET_PP_EQ;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_eq.direction = dir;
    pp_eq.usecase = usecase;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_eq, sizeof(pp_eq));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    *ret_num_bands = pp_eq.ret_num_bands;
    memcpy(ret_value, &pp_eq.ret_preset_id , sizeof(audio_hal_plugin_bint32_t));
    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_get_pp_eq_subbands(struct cc_userdata *u,
                                                audio_hal_plugin_direction_type_t dir,
                                                audio_hal_plugin_usecase_type_t usecase,
                                                uint32_t num_bands,
                                                audio_hal_plugin_pp_eq_subband_binfo_t *ret_bands) {
    audio_hal_plugin_codec_get_pp_eq_subbands_t pp_eq_subbands;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_GET_PP_EQ_SUBBANDS;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    pp_eq_subbands.direction = dir;
    pp_eq_subbands.usecase = usecase;
    pp_eq_subbands.num_bands = num_bands;
    ret = u->audio_hal_plugin_send_msg(msg, (void*)&pp_eq_subbands, sizeof(pp_eq_subbands));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    memcpy(ret_bands, &pp_eq_subbands.ret_bands, num_bands * sizeof(audio_hal_plugin_pp_eq_subband_binfo_t));
    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_pp_tunnel_cmd(struct cc_userdata *u,
                                                uint8_t *input_data,
                                                uint32_t input_size,
                                                uint32_t expect_size,
                                                uint8_t *ret_data,
                                                uint32_t *ret_size) {
    audio_hal_plugin_codec_tunnel_get_t tunnel_get_cmd;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_TUNNEL_CMD;
    int ret = 0;

    pa_log_debug("%s begin", __FUNCTION__);

    tunnel_get_cmd.param_data = (int32_t *)input_data;
    tunnel_get_cmd.param_size = input_size;
    tunnel_get_cmd.size_to_get = expect_size;
    tunnel_get_cmd.ret_data = (int32_t *)ret_data;

    ret = u->audio_hal_plugin_send_msg(msg, (void*)&tunnel_get_cmd, sizeof(tunnel_get_cmd));
    if (ret) {
        pa_log_error("%s return Failure! %u: ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    *ret_size = tunnel_get_cmd.ret_size;
    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_extension_cb(pa_native_protocol *p,
                                                        pa_module *m,
                                                        pa_native_connection *c,
                                                        uint32_t tag,
                                                        pa_tagstruct *t) {
    struct cc_userdata *u;
    uint32_t command;
    pa_tagstruct *reply = NULL;
    audio_hal_plugin_usecase_type_t ext_usecase;
    audio_hal_plugin_direction_type_t ext_direction;
    cc_path_type_t path;
    cc_stream_direction_t direction;

    pa_assert(p);
    pa_assert(m);
    pa_assert(c);
    pa_assert(t);

    u = m->userdata;

    if (pa_tagstruct_getu32(t, &command) < 0)
        goto fail;
    pa_log_debug("%s  cmd %d IN", __FUNCTION__, command);

    reply = pa_tagstruct_new();
    pa_tagstruct_putu32(reply, PA_COMMAND_REPLY);
    pa_tagstruct_putu32(reply, tag);

    switch (command) {
        case SUBCOMMAND_TEST: {
            if (!pa_tagstruct_eof(t))
                goto fail;

            pa_tagstruct_putu32(reply, EXT_VERSION);
            break;
        }
        case SUBCOMMAND_SET_PP_VOLUME: {
            const char *role;
            uint8_t playback;
            uint32_t gain;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                (pa_tagstruct_getu32(t, &gain) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);
            if (audio_codec_set_pp_volume(u, ext_direction, ext_usecase, gain, 0))
                goto fail;
            break;
        }
        case SUBCOMMAND_SET_PP_MUTE: {
            const char *role;
            uint8_t playback;
            uint8_t mute;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                (pa_tagstruct_getu8(t, &mute) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_set_pp_mute(u, ext_direction, ext_usecase, mute, 0))
                goto fail;
            break;
        }
        case SUBCOMMAND_SET_PP_FADE: {
            const char *role;
            uint8_t playback;
            uint32_t fade;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                (pa_tagstruct_getu32(t, &fade) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_set_pp_fade(u, ext_direction, ext_usecase, (int32_t)fade))
                goto fail;
            break;
        }
        case SUBCOMMAND_SET_PP_BALANCE: {
            const char *role;
            uint8_t playback;
            uint32_t balance;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                (pa_tagstruct_getu32(t, &balance) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;
            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_set_pp_balance(u, ext_direction, ext_usecase, (int32_t)balance))
                goto fail;
            break;
        }
        case SUBCOMMAND_SET_PP_BMT: {
            const char *role;
            uint8_t playback;
            uint8_t filter_type; /**< Requested filter type */
            uint8_t enable_flag; /**< Enable flag. 0 - Disable, 1 - Enable */
            uint32_t value; /**< Requested value to be set */

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                (pa_tagstruct_getu8(t, &filter_type) < 0) ||
                (pa_tagstruct_getu8(t, &enable_flag) < 0) ||
                (pa_tagstruct_getu32(t, &value) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_set_pp_bmt(u, ext_direction, ext_usecase, filter_type, enable_flag, value))
                goto fail;
            break;
        }
        case SUBCOMMAND_SET_PP_EQ: {
            const char *role;
            uint8_t playback;
            uint8_t enable_flag; /**< Enable flag. 0 - Disable, 1 - Enable */
            uint32_t preset_id;
            uint32_t num_bands;
            uint32_t i;
            audio_hal_plugin_codec_pp_eq_subband_t *band_array;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                (pa_tagstruct_getu8(t, &enable_flag) < 0) ||
                (pa_tagstruct_getu32(t, &preset_id) < 0) ||
                (pa_tagstruct_getu32(t, &num_bands) < 0))
                goto fail;

            band_array = pa_xnew0(audio_hal_plugin_codec_pp_eq_subband_t, num_bands);
            for (i=0; i< num_bands; i++) {
                    pa_tagstruct_getu32(t, &band_array[i].band_idx);
                    pa_tagstruct_getu32(t, &band_array[i].center_freq);
                    pa_tagstruct_getu32(t, (uint32_t *) &band_array[i].band_level);
            }

            if(!pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_set_pp_eq(u, ext_direction, ext_usecase, enable_flag, preset_id, num_bands, band_array))
                goto fail;

            pa_xfree(band_array);
            break;
        }

        case SUBCOMMAND_GET_PP_VOLUME:
        {
            const char *role;
            uint8_t playback;
            audio_hal_plugin_buint32_t ret;

            memset(&ret, 0, sizeof(audio_hal_plugin_buint32_t));
            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_get_pp_volume(u, ext_direction, ext_usecase, 0, &ret))
                goto fail;

            pa_tagstruct_putu32(reply, ret.query_status_mask);
            pa_tagstruct_putu32(reply, ret.value);
            pa_tagstruct_putu32(reply, ret.min);
            pa_tagstruct_putu32(reply, ret.max);
            break;
        }
        case SUBCOMMAND_GET_PP_FADE:
        {
            const char *role;
            uint8_t playback;
            audio_hal_plugin_bint32_t ret;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_get_pp_fade(u, ext_direction, ext_usecase, 0, &ret))
                goto fail;

            pa_tagstruct_putu32(reply, ret.query_status_mask);
            pa_tagstruct_putu32(reply, (uint32_t)ret.value);
            pa_tagstruct_putu32(reply, (uint32_t)ret.min);
            pa_tagstruct_putu32(reply, (uint32_t)ret.max);
            break;
        } 
        case SUBCOMMAND_GET_PP_BALANCE:
        {
            const char *role;
            uint8_t playback;
            audio_hal_plugin_bint32_t ret;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_get_pp_balance(u, ext_direction, ext_usecase, 0, &ret))
                goto fail;

            pa_tagstruct_putu32(reply, ret.query_status_mask);
            pa_tagstruct_putu32(reply, (uint32_t)ret.value);
            pa_tagstruct_putu32(reply, (uint32_t)ret.min);
            pa_tagstruct_putu32(reply, (uint32_t)ret.max);
            break;
        }
        case SUBCOMMAND_GET_PP_BMT:
        {
            const char *role;
            uint8_t playback;
            uint8_t filter_type;
            audio_hal_plugin_bint32_t ret;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                (pa_tagstruct_getu8(t, &filter_type) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_get_pp_bmt(u, ext_direction, ext_usecase, filter_type, &ret))
                goto fail;

            pa_tagstruct_putu32(reply, ret.query_status_mask);
            pa_tagstruct_putu32(reply, (uint32_t)ret.value);
            pa_tagstruct_putu32(reply, (uint32_t)ret.min);
            pa_tagstruct_putu32(reply, (uint32_t)ret.max);
            break;
        }
        case SUBCOMMAND_GET_PP_EQ:
        {
            const char *role;
            uint8_t playback;
            audio_hal_plugin_bint32_t ret_preset_id;
            uint32_t ret_num_bands;

            memset(&ret_preset_id, 0, sizeof(audio_hal_plugin_buint32_t));
            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_get_pp_eq(u, ext_direction, ext_usecase, &ret_preset_id, &ret_num_bands))
                goto fail;

            pa_tagstruct_putu32(reply, ret_preset_id.query_status_mask);
            pa_tagstruct_putu32(reply, (uint32_t)ret_preset_id.value);
            pa_tagstruct_putu32(reply, (uint32_t)ret_preset_id.min);
            pa_tagstruct_putu32(reply, (uint32_t)ret_preset_id.max);
            pa_tagstruct_putu32(reply, (uint32_t)ret_num_bands);
            break;
        }
        case SUBCOMMAND_GET_PP_EQ_SUBBANDS:
        {
            const char *role;
            uint8_t playback;
            uint32_t num_bands;
            uint32_t i;
            audio_hal_plugin_pp_eq_subband_binfo_t *bands_info;

            if ((pa_tagstruct_gets(t, &role) < 0) ||
                (pa_tagstruct_getu8(t, &playback) < 0) ||
                (pa_tagstruct_getu32(t, &num_bands) < 0) ||
                !pa_tagstruct_eof(t))
                goto fail;

            bands_info = pa_xnew0(audio_hal_plugin_pp_eq_subband_binfo_t, num_bands);
            direction = playback ? AUDIO_PLAYBACK : AUDIO_CAPTURE;
            path = audio_codec_get_path(direction, role);
            ext_direction = audio_codec_get_ext_direction(direction);
            ext_usecase = audio_codec_get_ext_usecase(path);

            if (audio_codec_get_pp_eq_subbands(u, ext_direction, ext_usecase, num_bands, bands_info))
                goto fail;

            for (i=0; i< num_bands; i++) {
                pa_tagstruct_putu32(reply, bands_info[i].ret_center_freq.query_status_mask);
                pa_tagstruct_putu32(reply, (uint32_t)bands_info[i].ret_center_freq.value);
                pa_tagstruct_putu32(reply, (uint32_t)bands_info[i].ret_center_freq.min);
                pa_tagstruct_putu32(reply, (uint32_t)bands_info[i].ret_center_freq.max);
                pa_tagstruct_putu32(reply, bands_info[i].ret_band_level.query_status_mask);
                pa_tagstruct_putu32(reply, bands_info[i].ret_band_level.value);
                pa_tagstruct_putu32(reply, bands_info[i].ret_band_level.min);
                pa_tagstruct_putu32(reply, bands_info[i].ret_band_level.max);
            }
            break;
        }
        case SUBCOMMAND_TUNNEL_CMD:
        {
            uint8_t *input_data;
            uint32_t input_size;
            uint32_t expected_size;
            uint8_t *output_data;
            uint32_t output_size;
            uint32_t i;

            if (pa_tagstruct_getu32(t, &input_size) < 0)
                goto fail;

            input_data = pa_xnew0(uint8_t, input_size);
            for (i = 0; i < input_size; i++) {
                pa_tagstruct_getu8(t, &input_data[i]);
            }
            pa_tagstruct_getu32(t, &expected_size);
            output_data = pa_xnew0(uint8_t, expected_size);
            if (audio_codec_pp_tunnel_cmd(u, input_data, input_size, expected_size, output_data, &output_size))
                goto fail;

            if (output_size > expected_size)
                output_size = expected_size;

            pa_tagstruct_putu32(reply, output_size);
            if (output_size)
                for (i=0; i<output_size; i++)
                    pa_tagstruct_putu8(reply, output_data[i]);

            pa_xfree(input_data);
            pa_xfree(output_data);
            break;
        }

        default:
            goto fail;
    }
    pa_pstream_send_tagstruct(pa_native_connection_get_pstream(c), reply);
    
    pa_log_debug("%s cmd %dOUT", __FUNCTION__,command);
    return 0;

fail:
    pa_log_debug("%s cmd %d failure", __FUNCTION__,command);
    if (reply)
        pa_tagstruct_free(reply);

    return -1;
}


/* Enable codec and update the counter */
static int audio_codec_do_activate(struct cc_userdata *u,
        cc_stream_direction_t dir, const char *media_role, pa_object *obj) {
    int ret = -1;
    cc_path_desc_t *cp;
    uint32_t idx;
    audio_hal_plugin_usecase_type_t ext_usecase;
    audio_hal_plugin_direction_type_t ext_dir;

    /* Store codec path information */
    cp = pa_xnew0(cc_path_desc_t, 1);
    if (!cp) {
        pa_log_error("%s Failed to allocate cc_codec_t node", __FUNCTION__);
        return ret;
    }

    cp->dir = dir;
    cp->path = audio_codec_get_path(dir, media_role);
    cp->object = obj;

    pa_log_debug("%s begin media.role %s direction %d  ", __FUNCTION__, media_role, dir);
    if (-1 == pa_idxset_put(u->codec_list, cp, &idx)) {
        pa_log_debug("%s codec path %d already enabled", __FUNCTION__, cp->path);
        ret = 0;
    } else {
        ext_usecase = audio_codec_get_ext_usecase(cp->path);
        ext_dir = audio_codec_get_ext_direction(cp->dir);
        ret = audio_codec_set_enable(NULL, NULL, u, ext_dir, ext_usecase);
    }

    pa_log_debug("%s end %s media.role %s direction %d codec path %d list size: %d", __FUNCTION__,
            (ret == 0) ? "success":"failure",
             media_role,
             dir,
             cp->path,
             pa_idxset_size(u->codec_list));
end:
    return ret;
}

/* Disable codec and update the counter. Return 1 if not find codec in list */
static int audio_codec_do_inactivate(struct cc_userdata *u, pa_object *obj,
        cc_stream_direction_t dir, const char *media_role) {
    cc_path_desc_t *cp, *tmp = NULL;
    uint32_t idx;
    int ret = -1;
    audio_hal_plugin_usecase_type_t ext_usecase;
    audio_hal_plugin_direction_type_t ext_dir;

    if (pa_idxset_isempty(u->codec_list)) {
        pa_log_debug("%s no active codec path", __FUNCTION__);
        return ret;
    }

    pa_log_debug("%s begin media.role %s direction %d list size: %d", __FUNCTION__,
                    media_role,
                    dir,
                    pa_idxset_size(u->codec_list));

    PA_IDXSET_FOREACH(cp, u->codec_list, idx) {
        if (cp->object != NULL) {
            if (cp->object == obj) {
                ext_usecase = audio_codec_get_ext_usecase(cp->path);
                ext_dir = audio_codec_get_ext_direction(cp->dir);
                ret = audio_codec_set_disable(NULL, NULL, u, ext_dir, ext_usecase);
            }
        } else {
            cc_path_type_t path = audio_codec_get_path(dir, media_role);

            if (dir == cp->dir && path == cp->path) {
                ext_usecase = audio_codec_get_ext_usecase(cp->path);
                ext_dir = audio_codec_get_ext_direction(cp->dir);
                ret = audio_codec_set_disable(NULL, NULL, u, ext_dir, ext_usecase);
            }
        }

        if (!ret) {
            tmp = pa_idxset_remove_by_index(u->codec_list, idx);
             if (tmp)
                 pa_xfree(tmp);
             break;
       }
    }

    pa_log_debug("%s end media.role %s %s list size: %d", __FUNCTION__, media_role,
            (ret == 0) ? "success":"failure",
            pa_idxset_size(u->codec_list));

    /* Add log for automation judgement */
    if ((ret >= 0) && !pa_idxset_size(u->codec_list))
        pa_log_info("%s codec control for %s succeed!", __FUNCTION__, media_role);
    return ret;
}

static pa_hook_result_t sink_input_put_hook_callback(pa_core *c, pa_sink_input *sink_input, struct cc_userdata *u) {
    const char *role;
    int ret = PA_HOOK_OK;

    pa_log_debug("%s begin", __FUNCTION__);

    /* get media role */
    role = pa_proplist_gets(sink_input->proplist, PA_PROP_MEDIA_ROLE);
    if ((role != NULL) && !strcmp(role, "abstract"))
        return ret;

    /* Stream playback began, enable the codec */
    ret = audio_codec_do_activate(u, AUDIO_PLAYBACK, role, PA_OBJECT(sink_input->sink));
    if (ret < 0) {
        pa_log_error("%s activate codec failure, role=%s", __FUNCTION__, role);
        return PA_HOOK_CANCEL;
    } else {
        pa_log_debug("%s media role %s end successfully", __FUNCTION__, role);
        return PA_HOOK_OK;
    }
}

static pa_hook_result_t sink_input_unlink_hook_callback(pa_core *c, pa_sink_input *sink_input, struct cc_userdata *u) {
    return PA_HOOK_OK;
}

static pa_hook_result_t source_output_put_hook_callback(pa_core *c, pa_source_output *source_output, struct cc_userdata *u) {
    const char *role;
    int ret = PA_HOOK_OK;

    pa_log_debug("%s begin", __FUNCTION__);

    /* get media role */
    role = pa_proplist_gets(source_output->proplist, PA_PROP_MEDIA_ROLE);
	pa_log_info("%s:role=%s\n",__FUNCTION__,role);
    if ((role != NULL) && !strcmp(role, "abstract"))
        return ret;

    /* CODEC path does not need to be enabled/disabled for the capture path of
     * SDARS tuner use case. The tuner data feeds directly into the DSP from
     * the tuner HW.
     */
    if ((role != NULL) && !strcmp(role, "sdars_tuner_local_amp"))
        return ret;
    if ((role != NULL) && pa_streq(role, "voice_ecns")){
        ret = audio_codec_do_activate(u, AUDIO_CAPTURE, "voice_ecns_ref", PA_OBJECT(source_output->source));
        pa_log_debug("EC_REF_CAPTURE activated:%d\n",ret);
        if (ret)
            pa_log_error("EC_REF_CAPTURE activation ERROR:%d\n",ret);
    }
    /* Stream capture began, enable the codec */
    ret = audio_codec_do_activate(u, AUDIO_CAPTURE, role, PA_OBJECT(source_output->source));
    if (ret < 0) {
        pa_log_error("%s activate codec failure, role=%s", __FUNCTION__, role);
        return PA_HOOK_CANCEL;
    } else {
        pa_log_debug("%s media role %s end successfully", __FUNCTION__, role);
        return PA_HOOK_OK;
    }
}

static pa_hook_result_t source_output_unlink_hook_callback(pa_core *c, pa_source_output *source_output, struct cc_userdata *u) {
    return PA_HOOK_OK;
}

static pa_hook_result_t device_state_changed_hook_callback(pa_core *c, pa_object *o, struct cc_userdata *u) {
    const char *dev_class, *role;
    int ret = PA_HOOK_OK;
    pa_sink_input *sinp;
    pa_source_output *sout;
    uint32_t index;

    pa_assert(c);
    pa_object_assert_ref(o);
    pa_assert(u);

    pa_log_debug("%s begin", __FUNCTION__);

    if (pa_sink_isinstance(o)) {
        pa_sink *s = PA_SINK(o);
        pa_sink_state_t state = pa_sink_get_state(s);

        dev_class = pa_proplist_gets(s->proplist, PA_PROP_DEVICE_CLASS);
        if (!dev_class) {
            pa_log("Invalid sink dev_class!");
            return ret;
        }
        if (pa_streq(dev_class, "abstract") || pa_streq(dev_class, "monitor")) {
            pa_log_debug("%s skip dev_class %s sink name %s", __FUNCTION__, dev_class, s->name);
            return ret;
        }
        if (s->state != PA_SINK_SUSPENDED) {
            pa_log_debug("%s skip. not PA_SINK_SUSPENDED. sink state is %d name %s", __FUNCTION__, s->state, s->name);
            return ret;
        }

        PA_IDXSET_FOREACH(sinp, c->sink_inputs, index) {
                if(sinp && sinp->sink->index == s->index){
                        pa_log_info("Stream still on the sink, keep codec enabled.");
                        return ret;
                }
        }
        pa_log_info("Sink is suspended and no stream on it, disable the codec.");
        role = pa_proplist_gets(s->proplist, PA_PROP_MEDIA_ROLE);
        ret = audio_codec_do_inactivate(u, o, AUDIO_PLAYBACK, role);
    } else if (pa_source_isinstance(o)) {
        pa_source *s = PA_SOURCE(o);
        pa_source_state_t state = pa_source_get_state(s);

        dev_class = pa_proplist_gets(s->proplist, PA_PROP_DEVICE_CLASS);
        if (!dev_class) {
            pa_log("Invalid source dev_class!");
            return ret;
        }
        if (pa_streq(dev_class, "abstract") || pa_streq(dev_class, "monitor")) {
            pa_log_debug("%s skip dev_class %s source name %s", __FUNCTION__, dev_class, s->name);
            return ret;
        }

        if (s->state != PA_SOURCE_SUSPENDED) {
            pa_log_debug("%s skip.not PA_SOURCE_SUSPENDED. source state is %d name %s", __FUNCTION__, s->state, s->name);
            return ret;
        }

        PA_IDXSET_FOREACH(sout, c->source_outputs, index) {
                if(sout && sout->source->index == s->index){
                        pa_log_info("Stream still on the source, keep codec enabled.");
                        return ret;
                }
        }
        pa_log_info("Source is suspended and no stream on it, disable the codec.");
        role = pa_proplist_gets(s->proplist, PA_PROP_MEDIA_ROLE);

        if ((role != NULL) && pa_streq(role, "voice_ecns")){
            ret = audio_codec_do_inactivate(u, o, AUDIO_CAPTURE, "voice_ecns_ref");
            pa_log_debug("EC_REF_CAPTURE inactivated:%d\n",ret);
            if (ret)
                pa_log_error("EC_REF_CAPTURE inactivation ERROR:%d\n",ret);
        }
        ret = audio_codec_do_inactivate(u, o, AUDIO_CAPTURE, role);

    }

    pa_log_debug("%s end successfully", __FUNCTION__);
    return ret;
}

static int audio_codec_activate_once(struct cc_userdata *u)
{
    int ret = 0;
    int index = 0;

    /* Activate codecs before the alsa sink/source modules loading */
    cc_stream_direction_t dir = AUDIO_PLAYBACK;

    const char *role = "music";
    ret = audio_codec_do_activate(u, dir, role, NULL);
    role = "navi";
    ret |= audio_codec_do_activate(u, dir, role, NULL);
    role = "sdars_tuner_local_amp";
    ret |= audio_codec_do_activate(u, dir, role, NULL);
    role = "hfp_wb_downlink";
    ret |= audio_codec_do_activate(u, dir, role, NULL);
    role = "hfp_wb_uplink";
    ret |= audio_codec_do_activate(u, dir, role, NULL);
    //role = "icc_call";
    //ret |= audio_codec_do_activate(u, dir, role, NULL);
    dir = AUDIO_CAPTURE;
    role = "sdars_tuner_local_amp";
    ret |= audio_codec_do_activate(u, dir, role, NULL);
    role = "record";
    ret |= audio_codec_do_activate(u, dir, role, NULL);
    role = "hfp_wb_downlink";
    ret |= audio_codec_do_activate(u, dir, role, NULL);
    role = "hfp_wb_uplink";
    ret |= audio_codec_do_activate(u, dir, role, NULL);
    //role = "icc_call";
    //ret |= audio_codec_do_activate(u, dir, role, NULL);
    return ret;
}

int pa__init(pa_module*m) {
    struct cc_userdata *u;
    int32_t ret = 0;

    pa_log_info("Start Loading Codec Control module");

    pa_assert(m);

    m->userdata = u = pa_xnew0(struct cc_userdata, 1);
    u->core = m->core;
    u->module = m;

    u->plugin_handle = dlopen(LIB_PLUGIN_DRIVER, RTLD_NOW);
    if (u->plugin_handle == NULL) {
        pa_log_error("DLOPEN failed for %s %s", LIB_PLUGIN_DRIVER, dlerror());
        goto fail;
    } else {
        pa_log_debug("DLOPEN successful for %s", LIB_PLUGIN_DRIVER);
        u->audio_hal_plugin_init = (audio_hal_plugin_init_t)dlsym(
                        u->plugin_handle, "audio_hal_plugin_init");
        if (!u->audio_hal_plugin_init) {
            pa_log_error("Could not find the symbol audio_hal_plugin_init from %s",LIB_PLUGIN_DRIVER);
            goto fail;
        }

        u->audio_hal_plugin_deinit = (audio_hal_plugin_deinit_t)dlsym(
                        u->plugin_handle, "audio_hal_plugin_deinit");
        if (!u->audio_hal_plugin_deinit) {
            pa_log_error("Could not find the symbol audio_hal_plugin_deinit from %s", LIB_PLUGIN_DRIVER);
            goto fail;
        }

        u->audio_hal_plugin_send_msg = (audio_hal_plugin_send_msg_t)
                        dlsym(u->plugin_handle, "audio_hal_plugin_send_msg");
        if (!u->audio_hal_plugin_send_msg) {
            pa_log_error("Could not find the symbol audio_hal_plugin_send_msg from %s", LIB_PLUGIN_DRIVER);
            goto fail;
        }

        ret = u->audio_hal_plugin_init();
        if (ret) {
            pa_log_error("audio_hal_plugin_init failed with ret = %d", ret);
            goto fail;
        }
    }


    pa_log_info("Load Codec Control Module Success!!!");
    return 0;
fail:
    if(u->plugin_handle != NULL)
        dlclose(u->plugin_handle);

    pa_log_info("Load Codec Control Module Failure!!!");
    return -1;
}

void pa__done(pa_module*m) {
    struct cc_userdata* u;
    int32_t ret = 0;

    pa_log_info("Start Unloading Codec Control Module");

    pa_assert(m);

    if (!(u = m->userdata))
        return;

    if (u->sink_input_put_hook_slot)
        pa_hook_slot_free(u->sink_input_put_hook_slot);

    if (u->sink_input_unlink_hook_slot)
        pa_hook_slot_free(u->sink_input_unlink_hook_slot);

    if (u->source_output_put_hook_slot)
        pa_hook_slot_free(u->source_output_put_hook_slot);

    if (u->source_output_unlink_hook_slot)
        pa_hook_slot_free(u->source_output_unlink_hook_slot);

    if (u->sink_state_changed_hook_slot)
        pa_hook_slot_free(u->sink_state_changed_hook_slot);

    if (u->source_state_changed_hook_slot)
        pa_hook_slot_free(u->source_state_changed_hook_slot);

    if (u->codec_list)
        pa_idxset_free(u->codec_list, pa_xfree);

    if (u->audio_hal_plugin_deinit) {
        ret = u->audio_hal_plugin_deinit();
        if (ret) {
            pa_log_error("audio_hal_plugin_deinit failed with ret = %d", ret);
        }
    }

    if (u->protocol) {
        pa_native_protocol_remove_ext(u->protocol, m);
        pa_native_protocol_unref(u->protocol);
    }

    if(u->plugin_handle != NULL)
        dlclose(u->plugin_handle);

    pa_xfree(u);

    pa_log_info("Unload Codec Control Module Done");
}
