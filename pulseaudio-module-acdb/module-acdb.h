/***
Module_acdb.h -- Pulseaudio module acdb is a module
belongs to pulseaudio, which could be loaded by pulseaudio.It used to
send audio calibration data when audio usecase setup.

Copyright (c) 2016-2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#ifndef fooacdbfoo
#define fooacdbfoo

#define LIB_ACDB_LOADER "libacdbloader.so"

#define ACDB_DEV_TYPE_OUT 1
#define ACDB_DEV_TYPE_IN  2

#define APP_ID_OUT 0x11130 /* Rx_Path */
#define APP_ID_IN  0x11132 /* Tx_Path */

#define MAX_CHANNEL 6

typedef int  (*acdb_init_t)(const char *, char *, int);
typedef void (*acdb_send_audio_cal_t)(int, int, int, int);
typedef void (*acdb_deallocate_t)();

typedef struct audio_usecase audio_usecase;

typedef struct {
    char *name;
    int dir;
    int dev_id;
    uint32_t rate;
    uint8_t minchannel;
    uint8_t maxchannel;
} acdb_group;

typedef struct {
    pa_hashmap *in;
    pa_hashmap *out;
}acdb_groupmap;

typedef struct {
    acdb_group **in[MAX_CHANNEL];
    acdb_group **out[MAX_CHANNEL];
}acdb_rolemap;

typedef enum {
    PCM_PLAYBACK,
    PCM_PLAYBACK_DRIVER_SIDE,
    PCM_CAPTURE,
    VOICE_CALL,
    HFP_UPLINK,
    HFP_DOWNLINK,
    HFP_WB_UPLINK,
    HFP_WB_DOWNLINK,
    HFP_UPLINK_NON_ECNS,
    HFP_DOWNLINK_NON_ECNS,
    HFP_WB_UPLINK_NON_ECNS,
    HFP_WB_DOWNLINK_NON_ECNS,
    ICC_CALL,
    SDARS_TUNER_LOCAL_AMP,
    A2DP_SINK_HOSTLESS_PRI,
    A2DP_SOURCE_PRI,
    PCM_CAPTURE_MCECNS,
    USECASE_END,
} usecase_type_t;

struct audio_usecase{
    usecase_type_t type;
    uint32_t sample_rate;
    uint8_t channels;
    int dir;
    int app_id;
    int acdb_dev_id;
};

#endif

