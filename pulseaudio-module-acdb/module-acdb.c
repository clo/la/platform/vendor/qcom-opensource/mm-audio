/***
Module_acdb.c -- Pulseaudio module acdb is a module
belongs to pulseaudio, which could be loaded by pulseaudio.It used to
send audio calibration data when audio usecase setup.

Copyright (c) 2016-2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <pulsecore/socket-client.h>
#include <pulsecore/module.h>
#include <pulsecore/modargs.h>
#include <pulsecore/core-util.h>

#include <stdio.h>
#include <dlfcn.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <json-c/json.h>

#include "module-acdb.h"

#ifndef DEFAULT_CONFIG_DIR
#define DEFAULT_CONFIG_DIR "/etc/pulse"
#endif

#ifndef DEFAULT_CONFIG_FILE
#define DEFAULT_CONFIG_FILE "pulseaudio-acdb.cfg"
#endif

PA_MODULE_AUTHOR("AGL audio");
PA_MODULE_DESCRIPTION("Load Audio Calibration Data When Audio Usecase Setup");
PA_MODULE_VERSION(PACKAGE_VERSION);
PA_MODULE_LOAD_ONCE(false);

static const char* const valid_modargs[] = {
    "config_dir",
    "config_file",
    NULL
};

struct userdata{
    pa_core *core;
    pa_module *module;
    pa_modargs *modargs;
    pa_hook_slot *sink_input_put_hook_slot,
                 *source_output_put_hook_slot;
    void *acdb_handle;
    acdb_init_t acdb_init;
    acdb_send_audio_cal_t acdb_send_audio_cal;
    acdb_deallocate_t acdb_deallocate;
    bool config_file_flag;
    acdb_groupmap *groupmap;
    acdb_rolemap *rolemap;
};

acdb_groupmap *acdb_groupmap_init(struct userdata *u)
{
    acdb_groupmap *gmap;

    gmap = pa_xnew0 (acdb_groupmap, 1);
    gmap->in = pa_hashmap_new (pa_idxset_string_hash_func, pa_idxset_string_compare_func);
    gmap->out = pa_hashmap_new (pa_idxset_string_hash_func, pa_idxset_string_compare_func);

    return gmap;
}

acdb_rolemap *acdb_rolemap_init(struct userdata *u)
{
    acdb_rolemap *rmap;
    int i;

    rmap = pa_xnew0(acdb_rolemap, 1);
    for (i = 0; i < MAX_CHANNEL; i++) {
        rmap->in[i] = NULL;
        rmap->out[i] = NULL;
    }

    return rmap;
}

void acdb_config_deinit(struct userdata *u)
{
    acdb_groupmap *gmap;
    acdb_group **rmap;
    int i;

    gmap = u->groupmap;
    pa_hashmap_free (gmap->in);
    pa_hashmap_free (gmap->out);

    for (i = 0; i < MAX_CHANNEL; i++) {
        if (rmap = u->rolemap->in[i])
            pa_xfree (rmap);
        if (rmap = u->rolemap->out[i])
            pa_xfree (rmap);
    }
    pa_xfree (u->groupmap);
    pa_xfree (u->rolemap);

    u->groupmap = NULL;
    u->rolemap = NULL;
}


usecase_type_t get_usecase_type_id(const char* role, int dir){
    usecase_type_t usecase_id;

    if (pa_streq(role, "music") || pa_streq(role, "event"))
        usecase_id = PCM_PLAYBACK;
    else if (pa_streq(role, "navi"))
        usecase_id = PCM_PLAYBACK_DRIVER_SIDE;
    else if (pa_streq(role, "record"))
        usecase_id = PCM_CAPTURE;
    else if (pa_streq(role, "voice_call"))
        usecase_id = VOICE_CALL;
    else if (pa_streq(role, "hfp_uplink"))
        usecase_id = HFP_UPLINK;
    else if (pa_streq(role, "hfp_downlink"))
        usecase_id = HFP_DOWNLINK;
    else if (pa_streq(role, "hfp_wb_uplink"))
        usecase_id = HFP_WB_UPLINK;
    else if (pa_streq(role, "hfp_wb_downlink"))
        usecase_id = HFP_WB_DOWNLINK;
    else if (pa_streq(role, "hfp_uplink_non_ecns"))
        usecase_id = HFP_UPLINK_NON_ECNS;
    else if (pa_streq(role, "hfp_downlink_non_ecns"))
        usecase_id = HFP_DOWNLINK_NON_ECNS;
    else if (pa_streq(role, "hfp_wb_uplink_non_ecns"))
        usecase_id = HFP_WB_UPLINK_NON_ECNS;
    else if (pa_streq(role, "hfp_wb_downlink_non_ecns"))
        usecase_id = HFP_WB_DOWNLINK_NON_ECNS;
    else if (pa_streq(role, "icc_call"))
        usecase_id = ICC_CALL;
    else if (pa_streq(role, "sdars_tuner_local_amp"))
        usecase_id = SDARS_TUNER_LOCAL_AMP;
    else if (pa_streq(role, "a2dp_sink_hostless_pri"))
        usecase_id = A2DP_SINK_HOSTLESS_PRI;
    else if (pa_streq(role, "a2dp_source_pri"))
        usecase_id = A2DP_SINK_HOSTLESS_PRI;
    else if (pa_streq(role, "voice_ecns"))
        usecase_id = PCM_CAPTURE_MCECNS;
    else{
        if (dir == ACDB_DEV_TYPE_OUT) {
            pa_log_info("%s:Using default PLAYBACK usecase type.",__func__);
            usecase_id = PCM_PLAYBACK;
        }
        else {
            /* dir == ACDB_DEV_TYPE_IN */
            pa_log_info("%s:Using default CAPTURE usecase type.",__func__);
            usecase_id = PCM_CAPTURE;
        }
    }

    return usecase_id;
}

acdb_group *acdb_create_topology_group (struct userdata *u, const char *name, const char *dev_type, uint32_t rate, int id)
{
    acdb_groupmap *gmap;
    acdb_group *group;

    pa_assert (u);
    pa_assert_se (gmap = u->groupmap);

    group = pa_xnew0 (acdb_group, 1);
    group->name = pa_xstrdup (name);
    group->dir = pa_streq(dev_type, "OUT") ? ACDB_DEV_TYPE_OUT : ACDB_DEV_TYPE_IN;
    group->rate = rate;
    group->dev_id = id;
    if (pa_streq(dev_type, "OUT"))
        pa_hashmap_put (gmap->out, group->name, group);
    else
        pa_hashmap_put (gmap->in, group->name, group);
    pa_log_debug ("acdb group '%s' created, device type is '%d', rate is %d, id is '%d'.", group->name, group->dir, group->rate, group->dev_id);

    return group;
}


bool acdb_map_role_to_group (struct userdata *u, const char *role, const char *dev_type, uint8_t minch, uint8_t maxch, const char *name)
{
    usecase_type_t usecase_id;
    int dir;
    pa_hashmap *gmap;
    acdb_group ***rmap;
    acdb_group **chmap;
    acdb_group *group;
    uint8_t ch;

    pa_assert (u);
    pa_assert (u->rolemap);
    pa_assert (u->groupmap);

    if (pa_streq(dev_type, "OUT")) {
        rmap = u->rolemap->out;
        gmap = u->groupmap->out;
    } else {
        rmap = u->rolemap->in;
        gmap = u->groupmap->in;
    }

    dir = pa_streq(dev_type, "OUT") ? ACDB_DEV_TYPE_OUT : ACDB_DEV_TYPE_IN;
    usecase_id = get_usecase_type_id(role, dir);
    group = pa_hashmap_get (gmap, name);

    minch = minch < 1 ? 1 : minch;
    maxch = maxch > MAX_CHANNEL ? MAX_CHANNEL : maxch;

    for (ch = minch-1; ch < maxch; ch++) {
        chmap = rmap[ch];
        if (!chmap) {
            chmap = pa_xnew0 (acdb_group *, USECASE_END);
            rmap[ch] = chmap;
        }

        chmap[usecase_id] = group;

    }
    pa_log_debug ("Mapping role '%s' with channels [%d:%d] to acdb group '%s'.", role, minch, maxch, name);

    return true;
}


bool acdb_config_file_parse (struct userdata *u, const char *path)
{
    bool result;
    int fd;
    struct stat filestat;
    void *fmap;
    struct json_object *fjson, *root, *sct, *elt, *tmp;
    int len, i;

    pa_assert (u);

    if (!path) return false;

    fd = open (path, O_RDONLY);
    if (fd == -1) {
        pa_log_info ("Could not find acdb configuration file '%s'.", path);
        return false;
    }
    fstat (fd, &filestat);

    fmap = mmap (NULL, filestat.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (fmap == MAP_FAILED) {
        pa_log_info ("Could not map configuration file in memory.");
        return false;
    }

    /* Start parsing JSON file */
    fjson = json_tokener_parse (fmap);
    json_object_object_get_ex (fjson, "config", &root);
    if (!fjson || !root) goto parse_failed;

    /* [groups] section */
    json_object_object_get_ex (root, "groups", &sct);
    if(!sct) goto parse_failed;
    len = json_object_array_length (sct);
    for (i=0; i<len; i++) {
        const char *name, *dev_type;
        uint32_t rate;
        int id;
        elt = json_object_array_get_idx (sct, i);
        json_object_object_get_ex (elt, "name", &tmp);
        if(!tmp) goto parse_failed;
        name = json_object_get_string (tmp);
        json_object_object_get_ex (elt, "dev_type", &tmp);
        if(!tmp) goto parse_failed;
        dev_type = json_object_get_string (tmp);
        json_object_object_get_ex (elt, "rate", &tmp);
        if(!tmp) goto parse_failed;
        rate = json_object_get_int (tmp);
        json_object_object_get_ex (elt, "dev_id", &tmp);
        if(!tmp) goto parse_failed;
        id = json_object_get_int (tmp);
        acdb_create_topology_group(u, name, dev_type, rate, id);
    }

    /* [rolemap] section */
    json_object_object_get_ex (root, "rolemap", &sct);
    if(!sct) goto parse_failed;
    len = json_object_array_length (sct);
    for (i=0; i<len; i++) {
        const char *media_role, *dev_type, *name;
        uint8_t minch, maxch;
        elt = json_object_array_get_idx (sct, i);
        json_object_object_get_ex (elt, "media_role", &tmp);
        if(!tmp) goto parse_failed;
        media_role = json_object_get_string (tmp);
        json_object_object_get_ex (elt, "dev_type", &tmp);
        if(!tmp) goto parse_failed;
        dev_type = json_object_get_string (tmp);
        json_object_object_get_ex (elt, "channels", &tmp);
        if(!tmp) goto parse_failed;
        minch = json_object_get_int (json_object_array_get_idx (tmp, 0));
        maxch = json_object_get_int (json_object_array_get_idx (tmp, 1));
        json_object_object_get_ex (elt, "group", &tmp);
        if(!tmp) goto parse_failed;
        name = json_object_get_string (tmp);
        acdb_map_role_to_group (u, media_role, dev_type, minch, maxch, name);
    }

    return true;

parse_failed:
    pa_log_info ("Parse configuration file failed.");
    return false;
}

int get_builtin_device_id(struct userdata *u, audio_usecase *usecase){
    int ret = 0;

    if (usecase->dir == ACDB_DEV_TYPE_OUT) {
        switch(usecase->type){
            case PCM_PLAYBACK:
            case SDARS_TUNER_LOCAL_AMP:
            case A2DP_SINK_HOSTLESS_PRI:
            case A2DP_SOURCE_PRI:
                usecase->acdb_dev_id = 41;
                usecase->sample_rate = 48000;
                break;
            case HFP_UPLINK:
            case HFP_UPLINK_NON_ECNS:
                usecase->acdb_dev_id = 21;
                usecase->sample_rate = 8000;
                break;
            case HFP_DOWNLINK:
            case HFP_WB_DOWNLINK:
            case HFP_DOWNLINK_NON_ECNS:
            case HFP_WB_DOWNLINK_NON_ECNS:
                usecase->acdb_dev_id = 15;
                usecase->sample_rate = 48000;
                break;
            case HFP_WB_UPLINK:
            case HFP_WB_UPLINK_NON_ECNS:
                usecase->acdb_dev_id = 39;
                usecase->sample_rate = 16000;
                break;
            case ICC_CALL:
                usecase->acdb_dev_id = 16;
                usecase->sample_rate = 48000;
                break;
            case PCM_PLAYBACK_DRIVER_SIDE:
                usecase->acdb_dev_id = 14;
                usecase->sample_rate = 48000;
                break;
            default:
                ret = -1;
                pa_log("No acdb id supported for usecase type (%d).", usecase->type);
        }
    }
    else {
    /* usecase->dir == ACDB_DEV_TYPE_IN */
        switch(usecase->type){
            case PCM_CAPTURE:
                if(usecase->channels == 1)
                    usecase->acdb_dev_id = 11;
                else
                    usecase->acdb_dev_id = 47;
                usecase->sample_rate = 48000;
                break;
            case HFP_UPLINK:
            case HFP_WB_UPLINK:
                usecase->acdb_dev_id = 11;
                usecase->sample_rate = 16000;
                break;
            case HFP_UPLINK_NON_ECNS:
            case HFP_WB_UPLINK_NON_ECNS:
                if (usecase->channels == 1)
                    usecase->acdb_dev_id = 48;
                if (usecase->channels == 2)
                    usecase->acdb_dev_id = 80;
                usecase->sample_rate = 16000;
                break;
            case HFP_DOWNLINK:
            case HFP_DOWNLINK_NON_ECNS:
                usecase->acdb_dev_id = 20;
                usecase->sample_rate = 8000;
                break;
            case HFP_WB_DOWNLINK:
            case HFP_WB_DOWNLINK_NON_ECNS:
                usecase->acdb_dev_id = 38;
                usecase->sample_rate = 16000;
                break;
            case ICC_CALL:
                usecase->acdb_dev_id = 46;
                usecase->sample_rate = 16000;
                break;
            case PCM_CAPTURE_MCECNS:
                usecase->acdb_dev_id = 12;
                usecase->sample_rate = 16000;
                break;
            default:
                ret = -1;
                pa_log("No acdb id supported for usecase type (%d).", usecase->type);
        }
    }
    return ret;
}

int get_config_device_id(struct userdata *u, audio_usecase *usecase){
    int ret = -1;
    pa_hashmap *gmap;
    acdb_group ***rmap;
    acdb_group **chmap;
    acdb_group *group = NULL;

    pa_assert (u);
    pa_assert (u->rolemap);
    pa_assert (u->groupmap);

    if (usecase->dir == ACDB_DEV_TYPE_OUT) {
        rmap = u->rolemap->out;
        gmap = u->groupmap->out;
    } else {
        rmap = u->rolemap->in;
        gmap = u->groupmap->in;
    }

    if(usecase->channels > MAX_CHANNEL)
        return -1;

    chmap = rmap[usecase->channels-1];
    if (chmap)
        group = chmap[usecase->type];

    if (group) {
        usecase->acdb_dev_id = group->dev_id;
        usecase->sample_rate = group->rate;
        ret = 0;
    }

    return ret;
}

int get_acdb_device_id(struct userdata *u, audio_usecase *usecase){
    int ret = -1;

    if(u->config_file_flag) {
        pa_log_info ("Apply acdb configuration file.");
        ret = get_config_device_id(u, usecase);
        if(ret) {
            pa_log_debug("No config for usecase id (%d) with direction (%d) and channel (%d), use builtin acdb id!", usecase->type, usecase->dir, usecase->channels);
            ret = get_builtin_device_id(u, usecase);
        }
    }
    else {
        pa_log_info ("Apply builtin default acdb id configuration.");
        ret = get_builtin_device_id(u, usecase);
    }

    return ret;
}



static pa_hook_result_t sink_input_put_hook_callback(pa_core *c, pa_sink_input *sink_input, struct userdata *udata){
    struct userdata *my_data = (struct userdata *)udata;
    audio_usecase *usecase;
    const char *role;
    int ret;

    pa_log_info("SINK_INPUT_PUT hook fired.");
    usecase = pa_xnew0 (audio_usecase, 1);

    usecase->dir = ACDB_DEV_TYPE_OUT;
    usecase->app_id = APP_ID_OUT;
    usecase->channels = sink_input->channel_map.channels;

    role = pa_proplist_gets(sink_input->proplist, PA_PROP_MEDIA_ROLE);
    if(!role)
        role = "none";
    pa_log_debug("Media role is (%s)", role);

    if (pa_streq(role, "abstract")){
        pa_log_info("Got abstract role, skip it.");
        goto end;
    }

    usecase->type = get_usecase_type_id(role, usecase->dir);
    ret = get_acdb_device_id(my_data, usecase);
    if (ret < 0) {
        pa_log("Get acdb device id for media role (%s) failed!", role);
        goto end;
    }

    /* send payload to acdbloader */
    pa_log_info("Sending audio calibration for media role (%s), usecase type is (%d), acdb_id is (%d)", role, usecase->type, usecase->acdb_dev_id);
    my_data->acdb_send_audio_cal(usecase->acdb_dev_id, usecase->dir, usecase->app_id, usecase->sample_rate);

end:
    pa_xfree (usecase);
    return PA_HOOK_OK;
}

static pa_hook_result_t source_output_put_hook_callback(pa_core *c, pa_source_output *source_output, struct userdata *udata){
    struct userdata *my_data = (struct userdata *)udata;
    audio_usecase *usecase;
    const char *role;
    int  ret;

    pa_log_info("SOURCE_OUTPUT_PUT hook fired.");
    usecase = pa_xnew0 (audio_usecase, 1);

    usecase->dir = ACDB_DEV_TYPE_IN;
    usecase->app_id = APP_ID_IN;
    usecase->channels = source_output->channel_map.channels;

    role = pa_proplist_gets(source_output->proplist, PA_PROP_MEDIA_ROLE);
    if(!role)
        role = "none";
    pa_log_debug("Media role is (%s)", role);

    if (pa_streq(role,"abstract")){
        pa_log_info("Got abstract role, skip it.");
        goto end;
    }

    /* The capture path for some hostless usecase  does not require calibration since
     * there will be no PP blocks used in the DSP.
     */
    if (pa_streq(role,"sdars_tuner_local_amp")||
             pa_streq(role,"a2dp_sink_hostless_pri")){
        pa_log_info("Got %s role, skip it.", role);
        goto end;
    }

    if (pa_streq(role,"voice_ecns")){
       usecase->acdb_dev_id = 100;
       usecase->sample_rate = 48000;
       pa_log_info("%s: sending audio calibration for voice_ecns_ref, acdb_id is (%d)", __func__, usecase->acdb_dev_id);
       my_data->acdb_send_audio_cal(usecase->acdb_dev_id, usecase->dir, usecase->app_id, usecase->sample_rate);
    }

    usecase->type = get_usecase_type_id(role, usecase->dir);
    ret = get_acdb_device_id(my_data, usecase);
    if (ret < 0) {
        pa_log("Get acdb device id for media role (%s) failed!", role);
        goto end;
    }
    /* send payload to acdbloader */
    pa_log_info("Sending audio calibration for media role (%s), usecase type is (%d), acdb_id is (%d)", role, usecase->type, usecase->acdb_dev_id);
    my_data->acdb_send_audio_cal(usecase->acdb_dev_id, usecase->dir, usecase->app_id, usecase->sample_rate);

end:
    pa_xfree(usecase);
    return PA_HOOK_OK;
}

int pa__init(pa_module* m)
{
    struct userdata *u;
    int ret_acdb_init;
    const char      *cfgdir;
    const char      *cfgfile;
    char cfgpath[1024];

    pa_log_info("Initializing \"acdb\" module");

    pa_assert(m);

    m->userdata = u = pa_xnew0(struct userdata,1);
    u->core = m->core;
    u->module = m;

    if(!(u->modargs = pa_modargs_new(m->argument, valid_modargs))){
        pa_log("Failed to parse module arguments.");
        goto fail;
    }

    u->groupmap = acdb_groupmap_init(u);
    u->rolemap = acdb_rolemap_init(u);

    cfgdir     = pa_modargs_get_value (u->modargs, "config_dir", DEFAULT_CONFIG_DIR);
    cfgfile    = pa_modargs_get_value (u->modargs, "config_file", DEFAULT_CONFIG_FILE);

    snprintf (cfgpath, sizeof(cfgpath), "%s/%s", cfgdir, cfgfile);
    pa_log_debug ("acdb configuration file path : %s", cfgpath);
    u->config_file_flag = acdb_config_file_parse(u, cfgpath);

    u->acdb_handle = dlopen(LIB_ACDB_LOADER,RTLD_NOW);
    if(u->acdb_handle ==NULL){
        pa_log("DLOPEN failed for %s, error is: %s.", LIB_ACDB_LOADER, dlerror());
        goto fail;
    }
    else
        pa_log_info("DLOPEN successful for %s.", LIB_ACDB_LOADER);

    u->acdb_init = (acdb_init_t)dlsym(u->acdb_handle, "acdb_loader_init_v2");
    if(!u->acdb_init) {
        pa_log("Could not find the symbol acdb_loader_init from %s.", LIB_ACDB_LOADER);
        goto fail;
    }

    ret_acdb_init = u->acdb_init(NULL, NULL, 0);
    if(ret_acdb_init) {
        pa_log("Error initializing ACDB returned = %d.", ret_acdb_init);
        goto fail;
    }
    else
        pa_log_info("Initialize ACDB successful!");

    u->acdb_send_audio_cal = (acdb_send_audio_cal_t)dlsym(u->acdb_handle, "acdb_loader_send_audio_cal_v2");
    if(!u->acdb_send_audio_cal) {
        pa_log("Could not find the symbol acdb_send_audio_cal from %s.", LIB_ACDB_LOADER);
        goto fail;;
    }

    u->acdb_deallocate = (acdb_deallocate_t)dlsym(u->acdb_handle, "acdb_loader_deallocate_ACDB");
    if(!u->acdb_deallocate) {
        pa_log("Could not find the symbol acdb_loader_deallocate_ACDB from %s.", LIB_ACDB_LOADER);
        goto fail;
    }

    u->sink_input_put_hook_slot = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SINK_INPUT_PUT], PA_HOOK_EARLY, (pa_hook_cb_t) sink_input_put_hook_callback, u);

    u->source_output_put_hook_slot = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SOURCE_OUTPUT_PUT], PA_HOOK_EARLY, (pa_hook_cb_t) source_output_put_hook_callback, u);

    return 0;

fail:
    return -1;
}

void pa__done(pa_module* m)
{
    struct userdata *u;

    pa_assert(m);
    u = m->userdata;

    if(u){

        if(u->modargs)
            pa_modargs_free(u->modargs);

        if(u->sink_input_put_hook_slot)
            pa_hook_slot_free(u->sink_input_put_hook_slot);

        if(u->source_output_put_hook_slot)
            pa_hook_slot_free(u->source_output_put_hook_slot);

        if(u->acdb_deallocate) {
            pa_log_info("Deallocating for ACDB ...");
            u->acdb_deallocate();
        }

        if(u->acdb_handle){
            dlclose(u->acdb_handle);
            pa_log_info("DLCLOSE successful for %s.", LIB_ACDB_LOADER);
        }

        if (u->groupmap || u->rolemap)
            acdb_config_deinit(u);
    }

    pa_xfree(u);

    pa_log_info("Closing \"acdb\" module");
}
